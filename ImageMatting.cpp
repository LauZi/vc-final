#include "ImageMatting.hpp"
#include <cstdio>
#include<vector>
#include "lodepng.h"
#include<queue>
#include<time.h>
#include<stdlib.h>
#define INF 100000000
#define SEARCH_RANGE 2
#define WIN_SIZE 1
#define BILAYER_TH_U 0.05
#define BILAYER_TH_D 0.04
//0.01
#define ERODE_WIN_SIZE 5
//3
#define ERODE_TH 85
//23
#define FOREGROUND 255
#define UNKNOWN 128
#define BACKGROUND 0

#define LATTICE 0.4
#define RELAX 1.5

inline double squareError3(double* a,double*b)
{
	return (a[0]-b[0])*(a[0]-b[0])+(a[1]-b[1])*(a[1]-b[1])+(a[2]-b[2])*(a[2]-b[2]);
}

void ImageMatting::initialize(double *img,unsigned int w,unsigned int h)
{
	back=img;
	W=w;
	H=h;
	fore=new double[W*H*3];
	trimap=new unsigned char[W*H];
	lapl=new double[W*H];
}

void ImageMatting::execute(double *img, double *rrr, const uint8 *mask)
{
	clock_t t=clock();
	frame=img;
	alpha=rrr;
	unknown.clear();

    getBilayer(mask);

	generateTrimap();
	t=clock()-t;
	printf("generate trimap:     %lf\n",(double)t/CLOCKS_PER_SEC);t=clock();

	estimateForeground();
	t=clock()-t;
	printf("estimate foreground: %lf\n",(double)t/CLOCKS_PER_SEC);t=clock();

	initializeAlpha();
	t=clock()-t;
	printf("initialize alpha:    %lf\n",(double)t/CLOCKS_PER_SEC);t=clock();

	calculateLaplacian();
	t=clock()-t;
	printf("calculate laplacian: %lf\n",(double)t/CLOCKS_PER_SEC);t=clock();

	solvePoisson();
	t=clock()-t;
	printf("solve poisson:       %lf\n",(double)t/CLOCKS_PER_SEC);

	system("pause");
}

void ImageMatting::getBilayer(const uint8 *mask)
{
    if (mask == NULL) {
	for(int i=0;i<H;i++)
		for(int j=0;j<W;j++)
		{
			double best=INF;
			for(int di=i-SEARCH_RANGE;di<=i+SEARCH_RANGE;di++)
				for(int dj=j-SEARCH_RANGE;dj<=j+SEARCH_RANGE;dj++)
				{
					double value=0;
					int cnt=0;
					for(int x=-WIN_SIZE;x<=WIN_SIZE;x++)
						for(int y=-WIN_SIZE;y<=WIN_SIZE;y++)
							if(i+x>=0&&i+x<H&&j+y>=0&&j+y<W&&di+x>=0&&di+x<H&&dj+y>=0&&dj+y<W)
							{
								value+=squareError3(&(back[((di+x)*W+dj+y)*3]),&(frame[((i+x)*W+j+y)*3]));
								cnt++;
							}
					if(cnt>0)
					{
						value/=cnt;
						if(value<best)best=value;
					}
				}
			if(best>BILAYER_TH_U)
				trimap[i*W+j]=1; // is fore
			else if(best<BILAYER_TH_D)trimap[i*W+j]=0; // is back
			else trimap[i*W+j]=2; // is unknown
		}
    } else {
        memcpy(trimap, mask, W * H);
    }

    #ifdef DEBUG
	{
		std::vector<unsigned char> tmp;
		for(int i=0;i<W*H;i++)
		{
			unsigned char v;
			if(trimap[i]==1)v=255;
			else if(trimap[i]==0)v=0;
			else v=128;
			tmp.push_back(v);
			tmp.push_back(v);
			tmp.push_back(v);
			tmp.push_back(255);
		}
		lodepng::encode("trimap-NoErode.png",tmp,W,H);
	}
    #endif
}

void ImageMatting::generateTrimap()
{
	for(int i=0;i<H;i++)
		for(int j=0;j<W;j++)
		{
			if(trimap[i*W+j]&2)continue;
			int cnt=0;
			for(int x=-ERODE_WIN_SIZE;x<=ERODE_WIN_SIZE;x++)
				for(int y=-ERODE_WIN_SIZE;y<=ERODE_WIN_SIZE;y++)
					if(i+x<H&&i+x>=0&&j+y<W&&j+y>=0)
					{
						if(trimap[(i+x)*W+j+y]&2)continue;
						else cnt+=(trimap[(i+x)*W+j+y]&1)?1:-1;
					}
			if(cnt>ERODE_TH)trimap[i*W+j]|=4; // is fore
			else if(cnt>=-ERODE_TH) trimap[i*W+j]|=8; // is unknown
		}
	for(int i=0;i<H;i++)
		for(int j=0;j<W;j++)
			if((trimap[i*W+j]&2)||(trimap[i*W+j]&8))
			{
				trimap[i*W+j]=128;
				POINT p;p.x=i;p.y=j;
				unknown.push_back(p);
			}
			else if(trimap[i*W+j]&4)trimap[i*W+j]=255;
			else trimap[i*W+j]=0;

    #ifdef DEBUG
	{
		std::vector<unsigned char> tmp;
		for(int i=0;i<H*W;i++)
		{
			tmp.push_back(trimap[i]);
			tmp.push_back(trimap[i]);
			tmp.push_back(trimap[i]);
			tmp.push_back(255);
		}
		lodepng::encode("trimap.png",tmp,W,H);
	}
    #endif
}

void ImageMatting::estimateForeground()
{
	bool *visited=new bool[W*H];
	std::queue<POINT> Q;
	for(int i=0;i<H;i++)
		for(int j=0;j<W;j++)
			if(trimap[i*W+j]==FOREGROUND)
			{
				fore[(i*W+j)*3]=frame[(i*W+j)*3];
				fore[(i*W+j)*3+1]=frame[(i*W+j)*3+1];
				fore[(i*W+j)*3+2]=frame[(i*W+j)*3+2];
				visited[i*W+j]=true;
				POINT p;p.x=i;p.y=j;
				Q.push(p);
			}
			else visited[i*W+j]=false;
	while(!Q.empty())
	{
		POINT now=Q.front();
		Q.pop();
		POINT next;

		next.x=now.x+1;
		next.y=now.y;
		if(next.x<H&&!visited[next.x*W+next.y])
		{
			fore[(next.x*W+next.y)*3]=fore[(now.x*W+now.y)*3];
			fore[(next.x*W+next.y)*3+1]=fore[(now.x*W+now.y)*3+1];
			fore[(next.x*W+next.y)*3+2]=fore[(now.x*W+now.y)*3+2];
			visited[next.x*W+next.y]=true;
			Q.push(next);
		}

		next.x=now.x-1;
		next.y=now.y;
		if(next.x>=0&&!visited[next.x*W+next.y])
		{
			fore[(next.x*W+next.y)*3]=fore[(now.x*W+now.y)*3];
			fore[(next.x*W+next.y)*3+1]=fore[(now.x*W+now.y)*3+1];
			fore[(next.x*W+next.y)*3+2]=fore[(now.x*W+now.y)*3+2];
			visited[next.x*W+next.y]=true;
			Q.push(next);
		}

		next.x=now.x;
		next.y=now.y+1;
		if(next.y<W&&!visited[next.x*W+next.y])
		{
			fore[(next.x*W+next.y)*3]=fore[(now.x*W+now.y)*3];
			fore[(next.x*W+next.y)*3+1]=fore[(now.x*W+now.y)*3+1];
			fore[(next.x*W+next.y)*3+2]=fore[(now.x*W+now.y)*3+2];
			visited[next.x*W+next.y]=true;
			Q.push(next);
		}

		next.x=now.x;
		next.y=now.y-1;
		if(next.y>=0&&!visited[next.x*W+next.y])
		{
			fore[(next.x*W+next.y)*3]=fore[(now.x*W+now.y)*3];
			fore[(next.x*W+next.y)*3+1]=fore[(now.x*W+now.y)*3+1];
			fore[(next.x*W+next.y)*3+2]=fore[(now.x*W+now.y)*3+2];
			visited[next.x*W+next.y]=true;
			Q.push(next);
		}
	}
	delete [] visited;

    #ifdef DEBUG
	{
		std::vector<unsigned char> tmp;
		for(int i=0;i<W*H;i++)
		{
			tmp.push_back((unsigned char)(fore[i*3]*255));
			tmp.push_back((unsigned char)(fore[i*3+1]*255));
			tmp.push_back((unsigned char)(fore[i*3+2]*255));
			tmp.push_back(255);
		}
		lodepng::encode("fore.png",tmp,W,H);
	}
    #endif
}

void ImageMatting::initializeAlpha()
{
	for(int i=0;i<H*W;i++)
	{
		if(trimap[i]==FOREGROUND)
			alpha[i]=1;
		else if(trimap[i]==BACKGROUND)
			alpha[i]=0;
		else
		{
			double r=fore[i*3]-back[i*3];
			double g=fore[i*3+1]-back[i*3+1];
			double b=fore[i*3+2]-back[i*3+2];
			double tmp=r*r+g*g+b*b;
			r=r*(frame[i*3]-back[i*3]);
			g=g*(frame[i*3+1]-back[i*3+1]);
			b=b*(frame[i*3+2]-back[i*3+2]);
			if(tmp==0)
				tmp=0;
			else
				tmp=(r+g+b)/tmp;
			if(tmp<0)tmp=0;
			if(tmp>1)tmp=1;
			alpha[i]=tmp;
		}
	}

    #ifdef DEBUG
	{
		std::vector<unsigned char> tmp;
		for(int i=0;i<W*H;i++)
		{
			tmp.push_back((unsigned char)(alpha[i]*255));
			tmp.push_back((unsigned char)(alpha[i]*255));
			tmp.push_back((unsigned char)(alpha[i]*255));
			tmp.push_back(255);
		}
		lodepng::encode("init.png",tmp,W,H);
	}
    #endif
}

void ImageMatting::calculateLaplacian()
{
	double *gx=new double[W*H];
	double *gy=new double[W*H];

	for(int i=0;i<H;i++)
		for(int j=0;j<W;j++)
		{
			double r,g,b;
			double r2,g2,b2;
			r2=fore[(i*W+j)*3]-back[(i*W+j)*3];
			g2=fore[(i*W+j)*3+1]-back[(i*W+j)*3+1];
			b2=fore[(i*W+j)*3+2]-back[(i*W+j)*3+2];

			if(i+1<H)
			{
				r=frame[((i+1)*W+j)*3];
				g=frame[((i+1)*W+j)*3+1];
				b=frame[((i+1)*W+j)*3+2];
			}
			else
			{
				r=frame[(i*W+j)*3];
				g=frame[(i*W+j)*3+1];
				b=frame[(i*W+j)*3+2];
			}
			if(i-1>=0)
			{
				r-=frame[((i-1)*W+j)*3];
				g-=frame[((i-1)*W+j)*3+1];
				b-=frame[((i-1)*W+j)*3+2];
			}
			else
			{
				r-=frame[(i*W+j)*3];
				g-=frame[(i*W+j)*3+1];
				b-=frame[(i*W+j)*3+2];
			}
			gx[i*W+j]=(r*r2+g*g2+b*b2)/(r2*r2+g2*g2+b2*b2);

			if(j+1<W)
			{
				r=frame[(i*W+j+1)*3];
				g=frame[(i*W+j+1)*3+1];
				b=frame[(i*W+j+1)*3+2];
			}
			else
			{
				r=frame[(i*W+j)*3];
				g=frame[(i*W+j)*3+1];
				b=frame[(i*W+j)*3+2];
			}
			if(j-1>=0)
			{
				r-=frame[(i*W+j-1)*3];
				g-=frame[(i*W+j-1)*3+1];
				b-=frame[(i*W+j-1)*3+2];
			}
			else
			{
				r-=frame[(i*W+j)*3];
				g-=frame[(i*W+j)*3+1];
				b-=frame[(i*W+j)*3+2];
			}
			gy[i*W+j]=(r*r2+g*g2+b*b2)/(r2*r2+g2*g2+b2*b2);
		}
	for(int i=0;i<H;i++)
		for(int j=0;j<W;j++)
		{
			double x,y;
			if(i+1<H)
				x=gx[(i+1)*W+j];
			else
				x=gx[i*W+j];
			if(i-1>=0)
				x-=gx[(i-1)*W+j];
			else
				x-=gx[i*W+j];

			if(j+1<W)
				y=gy[i*W+j+1];
			else
				y=gy[i*W+j];
			if(j-1>=0)
				y-=gy[i*W+j-1];
			else
				y-=gy[i*W+j];

			lapl[i*W+j]=x+y;
		}
	delete [] gx;
	delete [] gy;
}

void ImageMatting::solvePoisson()
{
	for(int t=0;t<64;t++)
		for(int k=0;k<unknown.size();k++)
		{
			POINT p=unknown[k];
			double x1,x2,y1,y2;
			if(p.x+1<H)x1=alpha[(p.x+1)*W+p.y];
			else x1=alpha[p.x*W+p.y];
			if(p.x-1>=0)x2=alpha[(p.x-1)*W+p.y];
			else x2=alpha[p.x*W+p.y];
			if(p.y+1<W)y1=alpha[p.x*W+p.y+1];
			else y1=alpha[p.x*W+p.y];
			if(p.y-1>=0)y2=alpha[p.x*W+p.y-1];
			else y2=alpha[p.x*W+p.y];
			x1=(x1+x2+y1+y2-LATTICE*lapl[p.x*W+p.y])*RELAX/4;
			alpha[p.x*W+p.y]=alpha[p.x*W+p.y]*(1-RELAX)+x1;
			if(alpha[p.x*W+p.y]>1)alpha[p.x*W+p.y]=1;
			if(alpha[p.x*W+p.y]<0)alpha[p.x*W+p.y]=0;
		}
}