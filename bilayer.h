#ifndef BILAYER_H
#define BILAYER_H

#include <cmath>
#include <cassert>

#include <vector>

#include "histo.h"
#include "gaussian.h"

typedef unsigned char uint8;

const double K_cont = 10000;

struct Picture {
    int height, width;
    std::vector<uint8> arr;

    Picture (int nheight, int nwidth, uint8 *pic):
        height(nheight), width(nwidth), arr(pic, pic + (height * width * 3)) {}

    Picture (): height(0), width(0) {}

    uint8& at(int i, int j, int k) { return this->at(i*width+j, k); }
    uint8& at(int idx, int k) { return arr[idx*3+k]; }

    uint8 get(int i, int j, int k) const { return this->get(i*width+j, k); }
    uint8 get(int idx, int k) const { return arr[idx*3+k]; }
};

class BackgroundModel {
public:
    const int height, width, N;
public:
    Picture background;
    Histogram hist;
public:
    double alpha;

    double diff(int x1, int y1, int x2, int y2) const {
        double s = 0;
        for (int i = 0; i < 3; ++i) s += sqr(background.get(x1, y1, i) - background.get(x2, y2, i));
        return s;
    }

    // p(I_r | x = 0)
    double global_p(const uint8 pxl[3]) const;
    // p_B(I_r)
    double local_p(const uint8 pxl[3], int i, int j) const;
    // p_{mix}(I_r)
    double mixed_p(const uint8 pxl[3], int i, int j) const {
        return alpha * global_p(pxl) + (1.0-alpha) * local_p(pxl, i, j) * 256;
    }

public:
    std::vector< Gaussian<3> > perpixel_model;

    void set_perpixel_model();

public:
    BackgroundModel (int nheight, int nwidth, uint8 *nbackground, int nK_b, double nalpha);
};


class ForegroundModel {
public:
    ForegroundModel (double n_learn_rate = 0.8):
            learn_rate(n_learn_rate), first_time(true) {
        assert (-1e6 < learn_rate and learn_rate < 1+1e6);
    }

    double p(uint8 r, uint8 g, uint8 b) const {
        uint8 pxl[3] = {r, g, b};
        return hist.p(pxl);
    }
    double p(uint8 *pxl) const { return hist.p(pxl); }

    double learn_rate;

    void update(const Picture &pic, const bool *is_foreground);
    void update(const Picture &pic, const uint8 *is_foreground) {
        update(pic, (const bool*) is_foreground);
    }
    void reset(const Picture &pic, const bool *is_foreground);
private:
    Histogram hist;
    bool first_time;
};


class CurrentModel {
public:
    Picture pic;

    CurrentModel (const Picture &n_pic): pic(n_pic) { calc_beta(); }
    CurrentModel (): beta(0) {}

    double dis_squared(int x, int y, int i, int j) const;
    double dis(int x, int y, int i, int j) const {
        return sqrt(dis_squared(x, y, i, j));
    }

    double cost(int x, int y, int i, int j, double norm = 1.0) const {
        return exp(-beta * dis(x, y, i, j) * norm);
    }
private:
    double beta;

    void calc_beta();
};

const double K_sqr_inv = 1.0 / K_cont / K_cont;
class FullModel {
public:
    BackgroundModel bg;
    ForegroundModel fg;
    CurrentModel cg;

    double lambda;

    double E_1(int i, int j, bool lbl) {
        uint8 pxl[3];
        for (int k = 0; k < 3; ++k)
            pxl[k] = cg.pic.at(i, j, k);

        return -log(lbl ? fg.p(pxl) : bg.mixed_p(pxl, i, j));
    }

    double E_2(int x, int y, int i, int j) const {
        double attenuation = 1.0 / (1.0 + bg.diff(x, y, i, j) * K_sqr_inv);
        return lambda * cg.cost(x, y, i, j, attenuation);
    }

    void set_initial_input(const Picture &input);

    uint8* solve_bilayer();

    FullModel(int height, int width, uint8 *background, int K_b, double alpha,
              double fg_learn_rate,
              double n_lambda);
    ~FullModel();

private:
    int *_data_cost;
    int *_smooth_hcost, *_smooth_vcost;

    uint8 *_output;
};


#endif
