#ifndef MYMATH_H
#define MYMATH_H

#include <cmath>

typedef unsigned char uint8;

#ifndef M_PI
const double M_PI = 2 * std::atan2(1, 0);
#endif

static const double eps = 1e-10;

inline bool is_zero(double d) { return d > 0 ? d < eps : d > -eps; }

template <class T>
inline T sqr(T n) { return n * n; }

inline uint8 crop_to_255(double d) {
    if (d > 255) return 255;
    if (d < 0) return 0;
    return (uint8) d;
}

uint8 *to_ycbcr(uint8 r, uint8 g, uint8 b);
uint8 *from_ycbcr(uint8 y, uint8 cb, uint8 cr);

inline uint8 *to_ycbcr(const uint8 *arr) { return to_ycbcr(arr[0], arr[1], arr[2]); }
inline uint8 *from_ycbcr(const uint8 *arr) { return from_ycbcr(arr[0], arr[1], arr[2]); }

#endif
