#include <cstdio>

#include "gaussian.h"

int main(int argc, char *argv[]) {
    Gaussian<3> g;

    double tmp[] = {0, 162, 232};
    double sigma[3][3] = {{0.0}};
    for (int i = 0; i < 3; ++i) {
        sigma[i][i] = 100;
    }

    g.set_mu(tmp);
    g.set_sigma(sigma);

    printf("%f\n", g.p(tmp));
    printf("sigma = %f\n", 1.0 / g.sigma_inv[0][0]);

    tmp[1] -= 10;
    printf("%f\n", g.p(tmp));

    tmp[1] -= 10;
    printf("%f\n", g.p(tmp));

    double sum = 0;
    for (int i = 0; i < 255; ++i) {
        tmp[0] = i;
        for (int j = 0; j < 255; ++j) {
            tmp[1] = j;
            for (int k = 0; k < 255; ++k) {
                tmp[2] = k;
                sum += g.p(tmp);
            }
        }
    }

    printf("sum = %f\n", sum);

    return 0;
}
