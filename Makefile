GCC = g++
FLAGS = -std=c++11 -O2

HFILES = bilayer.h gaussian.h histo.h bmpwriter.h mymath.h ImageMatting.hpp
CPPFILES = bilayer.cpp gaussian.cpp histo.cpp bmpwriter.cpp mymath.cpp ImageMatting.cpp

GCO_FILES = gco-v3.0/GCoptimization.cpp gco-v3.0/graph.cpp gco-v3.0/LinkedBlockList.cpp

main_video.exe: main_video.cpp ${HFIIES} ${CPPFILES}
	${GCC} ${FLAGS} ${GCO_FILES} lodepng.cpp ${CPPFILES} main_video.cpp -o main_video.exe

main.exe: main.cpp ${HFILES} ${CPPFILES}
	${GCC} ${FLAGS} ${GCO_FILES} lodepng.cpp ${CPPFILES} main.cpp -o main.exe

.PHONY = clear
clear:
	-@rm main.exe main_video.exe

test.exe: test.cpp gaussian.h
	${GCC} ${FLAGS} -o test.exe test.cpp
