#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <cstring>
#include <cassert>
#include <cmath>
#include <vector>

#include "gco-v3.0/GCoptimization.h"

#include "bilayer.h"
#include "bmpwriter.h"

const int BGM_LOCAL_RNG = 1;
const double gaussian_3_threshold = 0.00003;

using namespace std;

BackgroundModel::BackgroundModel (int nheight, int nwidth, uint8 *nbackground,
                                  int nK_b, double nalpha):

    height(nheight), width(nwidth), N(nheight * nwidth),
    background(Picture(height, width, nbackground)),
    alpha(nalpha) {

    bool *tmp = new bool [N];
    memset(tmp, 1, N);
    hist.set_histogram(nbackground, tmp, N);
    delete [] tmp;

    set_perpixel_model();
}

double BackgroundModel::global_p(const uint8 *pxl) const {
    return hist.p(pxl);
}

double BackgroundModel::local_p(const uint8 pxl[3], int x, int y) const {
    uint8 cy = to_ycbcr(&background.arr[(x*width+y)*3])[0];
    uint8 *ycbcr = to_ycbcr(pxl);

    ycbcr[0] = cy;

    uint8 *new_clr = from_ycbcr(ycbcr);

    double tmp[3];
    for (int i = 0; i < 3; ++i) tmp[i] = new_clr[i];

    return perpixel_model[x*width+y].p(tmp);
}

void BackgroundModel::set_perpixel_model() {
    perpixel_model.clear();

    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            double pxl[3];
            for (int k = 0; k < 3; ++k)
                pxl[k] = (double) background.get(i, j, k);

            double var = 0.0;
            double wt = 0.0;
            for (int a = -BGM_LOCAL_RNG; a <= BGM_LOCAL_RNG; ++a) {
                for (int b = -BGM_LOCAL_RNG; b <= BGM_LOCAL_RNG; ++b) {
                    int x = i + a, y = j + b;
                    if (x < 0 or x >= height) continue;
                    if (y < 0 or y >= width) continue;

                    double w = 1.0 / (1.0 + (double)(std::abs(a) + std::abs(b)));
                    wt += w;
                    for (int k = 0; k < 3; ++k)
                        var += sqr((double) background.get(x, y, k) - pxl[k]) * w;
                }
            }

            var /= wt * 3;

            if (var < 100) var = 100.0;

            double mat[3][3] = {{0.0}};
            for (int k = 0; k < 3; ++k) mat[k][k] = var;

            perpixel_model.push_back(Gaussian<3>(pxl, mat));
        }
    }
}


void ForegroundModel::update(const Picture &pic, const bool *is_foreground) {
    if (first_time) {
        first_time = false;
        reset(pic, is_foreground);
        return ;
    }

    Histogram *tmp = new Histogram;
    tmp->set_histogram(&pic.arr[0], is_foreground, pic.height * pic.width);

    double a = 1 - learn_rate, b = learn_rate;

    for (int i = 0; i < 256; ++i)
        for (int j = 0; j < 256; ++j)
            hist.histo[i][j] = hist.histo[i][j] * a + tmp->histo[i][j] * b;

    delete tmp;
}

void ForegroundModel::reset(const Picture &pic, const bool *is_foreground) {
    double learn_rate_backup = learn_rate;
    learn_rate = 1.0;

    update(pic, is_foreground);

    learn_rate = learn_rate_backup;
}



void CurrentModel::calc_beta() {
    int cnt = 0;
    double sum = 0.0;

    static const int dx[] = {1, 0, -1,  0, 1,  1, -1, -1};
    static const int dy[] = {0, 1,  0, -1, 1, -1,  1, -1};

    for (int i = 0; i < pic.height; ++i) {
        for (int j = 0; j < pic.width; ++j) {
            for (int k = 0; k < 4; ++k) {
                int x = i + dx[k], y = j + dy[k];
                if (x < 0 or x >= pic.height) continue;
                if (y < 0 or y >= pic.width) continue;

                cnt += 1;
                sum += dis_squared(i, j, x, y);
            }
        }
    }

    sum /= cnt;
    sum *= 2;

    beta = 1.0 / sum;
}

double CurrentModel::dis_squared(int x, int y, int i, int j) const {
    double sum = 0;
    for (int a = 0; a < 3; ++a) {
        double diff = (double)pic.get(i, j, a) - (double)pic.get(x, y, a);
        sum += diff * diff;
    }
    return sum;
}


FullModel::FullModel(int height, int width, uint8 *background, int K_b, double alpha,
                     double fg_learn_rate,
                     double n_lambda):
        bg(height, width, background, K_b, alpha),
        fg(fg_learn_rate),
        lambda(n_lambda),
        _output(NULL) {

    Picture tmp_pic;
    cg = CurrentModel(tmp_pic);

    _data_cost = new int [width*height*2];
    _smooth_hcost = new int [width*height];
    _smooth_vcost = new int [width*height];
}

FullModel::~FullModel () {
    delete [] _data_cost;
    delete [] _smooth_hcost;
    delete [] _smooth_vcost;
    delete [] _output;
}

void FullModel::set_initial_input(const Picture &input) {
    cg = CurrentModel(input);

    bool mask[input.height * input.width];

    int cnt = 0;
    for (int i = 0, idx = 0; i < input.height; ++i)
        for (int j = 0; j < input.width; ++j, ++idx)
            mask[idx] = bg.local_p(&input.arr[idx*3], i, j) < gaussian_3_threshold;

    #ifdef DEBUG
    BMPWriter *w = new BMPWriter(input.height, input.width, "mask.bmp");
    for (int idx = 0; idx < input.height * input.width; ++idx) {
        if (mask[idx])
            w->write_pxl(255, 255, 255);
        else
            w->write_pxl(0, 0, 0);
    }
    delete w;
    #endif DEBUG

    fg.reset(input, mask);
}

uint8* FullModel::solve_bilayer() {
    int smooth_cost_lbl[4] = {0, 1, 1, 0};
    const int height = cg.pic.height, width = cg.pic.width;

    GCoptimizationGridGraph opt(width, height, 2);

    for (int i = 0, idx = 0; i < height; ++i)
        for (int j = 0; j < width; ++j)
            for (int k = 0; k < 2; ++k, ++idx)
                _data_cost[idx] = (int)E_1(i, j, k);

    opt.setDataCost(_data_cost);

    std::vector<int> smooth_hcost(height * width), smooth_vcost(height * width);

    for (int i = 0, idx = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j, ++idx) {
            smooth_hcost[idx] = j+1 == width  ? 0 : (int)E_2(i, j, i, j+1);
            smooth_vcost[idx] = i+1 == height ? 0 : (int)E_2(i, j, i+1, j);
        }
    }

    opt.setSmoothCostVH(smooth_cost_lbl, &smooth_vcost[0], &smooth_hcost[0]);

    opt.swap(20); // iteratively solve for at most 20 times

    if (_output == NULL)
        _output = new uint8 [height * width];

    for (int i = 0; i < height*width; ++i)
        _output[i] = opt.whatLabel(i);

    return _output;
}
