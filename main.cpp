#include <cstdio>
#include <string>

#include "bilayer.h"
#include "bmpwriter.h"
#include "lodepng.h"

#include "gco-v3.0/GCoptimization.h"

unsigned int width, height;
void load_image(std::vector<uint8> &image, std::string file_name) {
	lodepng::decode(image, width, height, std::string(file_name));

	for (unsigned int i = 0, idx = 0; i < width * height; ++i, ++idx)
		for (int j = 0; j < 3; ++j, ++idx)
			image[i*3+j] = image[idx];
}

void run_shit(std::vector<uint8> &bg_image,
              std::vector<uint8> &fg_image,
              std::string out_file_name,
              double alpha, double lambda) {

    printf("Alpha = %f, lambda = %f\n", alpha, lambda);

	FullModel mdl(height, width, &bg_image[0], 10,
		          alpha, // alpha
                  0.8, // fg learn rate, not used in this case
                  lambda); // lambda

    puts("MADE BACKGROUND MODEL");

    mdl.set_initial_input(Picture(height, width, &fg_image[0]));

    bool *output;
    try {
        output = (bool*) mdl.solve_bilayer();
    } catch (GCException e) {
        fprintf(stderr, "GCExceoption:");
        e.Report();
    }

    puts("SOLVED BILAYER");

    BMPWriter *bmpwriter = new BMPWriter(height, width, out_file_name.c_str());

	for (unsigned int i = 0, idx = 0; i < width * height; ++i, ++idx) {
        if (output[idx])
            bmpwriter->write_pxl(255, 255, 255);
        else
            bmpwriter->write_pxl(0, 0, 0);
	}

    puts("MADE TMP BUFFER");

    delete bmpwriter;

    puts("FILE WRITTEN");
}

int main(int argc, char *argv[]) {
    std::string bg_file_name, fg_file_name;
    double alpha = 0.5, lambda = 5.0;
	if (argc < 1+2) {
		puts("Usage: ./main bg.png fg1.png alpha lambda");
        puts("Setting file names to default values.");

        bg_file_name = "bg3.png";
        fg_file_name = "fg3.png";
	} else {
        bg_file_name = argv[1];
        fg_file_name = argv[2];

        if (argc >= 4)
            sscanf(argv[3], "%lf", &alpha);
        if (argc >= 5)
            sscanf(argv[4], "%lf", &lambda);
    }

    std::vector<uint8> bg_image, in_image;

    load_image(bg_image, bg_file_name);
    load_image(in_image, fg_file_name);

    printf("Picture height = %d, width = %d\n", height, width);

    run_shit(bg_image, in_image, "out.bmp", alpha, lambda);

    return 0;
}
