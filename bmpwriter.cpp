#include "bmpwriter.h"

void BMPWriter::write_pxl(uint8 r, uint8 g, uint8 b) {
    int idx = _x++ * 3;
    _out_bfr[idx++] = b;
    _out_bfr[idx++] = g;
    _out_bfr[idx++] = r;

    if (_x == width) {
        _write(_out_bfr, 1, _bfr_len);
        _x = 0;
    }
}

void BMPWriter::_write_header() {
    _write("BM", 1, 2);
    int bfOffBits = 2+4+2+2+4+40;
    int size = bfOffBits + height * _bfr_len;
    int tmp = 0;

    _write(&size, 4, 1);
    _write(&tmp, 4, 1);
    _write(&bfOffBits, 4, 1);

    tmp = 40;

    _write(&tmp, 4, 1);
    _write(&width, 4, 1);
    int tmp_height = -height;
    _write(&tmp_height, 4, 1);

    int16 tmp16 = 1;
    _write(&tmp16, 2, 1);
    tmp16 = 24;
    _write(&tmp16, 2, 1);

    tmp = 0;
    for (int i = 0; i < 6; ++i)
        _write(&tmp, 4, 1);
}
