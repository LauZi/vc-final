#include <cstdio>
#include <cstdlib>
#include <vector>

#include "lodepng.h"

#include "gco-v3.0/GCoptimization.h"

#include "ImageMatting.hpp"

#include "bilayer.h"
#include "lodepng.h"


double *back;
double *fore;
double *result;
unsigned int W,H;

void readImage(double **img,unsigned int& w,unsigned int& h, const char *path)
{
	std::vector<unsigned char> tmp;
	lodepng::decode(tmp,w,h,path);

    if (*img == NULL)
        (*img)=new double[W*H*3];

	for(int i=0;i<W*H;i++)
	{
		(*img)[i*3]=((double)tmp[i*4])/255;
		(*img)[i*3+1]=((double)tmp[i*4+1])/255;
		(*img)[i*3+2]=((double)tmp[i*4+2])/255;
	}
}

std::vector<unsigned char> newbg;
std::vector<unsigned char> newfg;
int main()
{
	/////////////////////////
	///// Video Version /////
	/////////////////////////

	char bstr[100]="input/input (1).png";
	char fstr[100];
	char ostr[100];

	readImage(&back,W,H,bstr);

	result=new double[W*H];

	lodepng::decode(newbg,W,H,"newbg.png");

    std::vector<uint8> uint8_bfr(W * H * 3);
    for (int i = 0; i < W * H * 3; ++i)
        uint8_bfr[i] = (uint8) (back[i] * 255);

    FullModel mdl(H, W, &uint8_bfr[0], 10, 0.5, 0.8, 6);

	ImageMatting im;
	im.initialize(back,W,H);

    readImage(&fore, W, H, "input/input (24).png");
    for (int i = 0; i < W * H * 3; ++i)
        uint8_bfr[i] = (uint8) (fore[i] * 255);
    mdl.set_initial_input(Picture(270, 480, &uint8_bfr[0]));

	for(int f=1;f<=41;f++)
	{
        printf("Frame %d:\n", f);

		sprintf(fstr,"input/input (%d).png",f);
		sprintf(ostr,"output/output (%d).png",f);

		readImage(&fore,W,H,fstr);
        for (int i = 0; i < W * H * 3; ++i)
            uint8_bfr[i] = (uint8) (fore[i] * 255);

        Picture pic(H, W, &uint8_bfr[0]);
        mdl.cg = CurrentModel(pic);
        uint8 *init_mask = mdl.solve_bilayer();

		im.execute(fore, result, init_mask);

        for (int i = 0; i < W * H; ++i)
            uint8_bfr[i] = round(result[i]);

        //mdl.fg.update(pic, &uint8_bfr[0]);

		newfg.clear();
		for(int i=0;i<H;i++)
			for(int j=0;j<W;j++)
			{
                int idx = i * W + j;
                for (int k = 0; k < 3; ++k)
                {
                    double val = fore[idx * 3 + k]*result[idx]*255.0;
                    val += newbg[idx * 4 + k] * (1 - result[idx]);
                    if (val > 255) val = 255;
                    if (val < 0) val = 0;
                    newfg.push_back((uint8) val);
                }

                newfg.push_back(255);
			}

		lodepng::encode(ostr,newfg,W,H);
	}

    return 0;
}