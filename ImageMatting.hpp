#ifndef IMAGE_MATTING
#define IMAGE_MATTING
#include <cstdio>
#include<vector>
struct POINT{int x,y;};

typedef unsigned char uint8;

struct ImageMatting
{
public:
	void initialize(double *img,unsigned int w,unsigned int h);
	void execute(double *img,double *result, const uint8 *mask);

    ~ImageMatting() {
        delete [] fore;
        delete [] trimap;
        delete [] lapl;
    }
private:
	unsigned int W,H;
	double *back;
	double *fore;
	double *frame;
	double *alpha;
	uint8 *trimap;
	double *lapl;
	std::vector<POINT> unknown;

    void getBilayer(const uint8 *mask=NULL);
	void generateTrimap();
	void estimateForeground();
	void initializeAlpha();
	void calculateLaplacian();
	void solvePoisson();
};

#endif