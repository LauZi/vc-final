# VC Project #
- `gco-v3.0/` is from [here](http://vision.csd.uwo.ca/code/) and is for energy minimization.
- `lodepng.h/cpp` is from [here](http://lodev.org/lodepng/). It is used for reading/writing pngs.
- `main.cpp` runs bilayer segmentation on a single picture given background and foreground.
  The results are put under the same folder as `main.exe`. Compile and run for hints of usage.
- `main_video.cpp` runs the whole algorithm on a series of pictures.
  The old background frame, foreground frame (for simplicity), and new background image
  are specified in this file.
