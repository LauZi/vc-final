#include "mymath.h"

uint8 *to_ycbcr(uint8 rp, uint8 gp, uint8 bp) {
    static uint8 ans[3];

    double r = rp, g = gp, b = bp;

    ans[0] = crop_to_255( 0.299 * r + 0.587 * g + 0.114 * b      );
    ans[1] = crop_to_255(-0.168 * r - 0.331 * g + 0.500 * b + 128);
    ans[2] = crop_to_255( 0.500 * r - 0.419 * g - 0.081 * b + 128);

    return ans;
}

uint8 *from_ycbcr(uint8 yp, uint8 cbp, uint8 crp) {
    static uint8 ans[3];

    double y = yp, cb = cbp - 128, cr = crp - 128;

    ans[0] = crop_to_255(y                + 1.402   * cr);
    ans[1] = crop_to_255(y - 0.34414 * cb - 0.71414 * cr);
    ans[2] = crop_to_255(y + 1.771   * cb               );

    return ans;
}
