#ifndef GAUSSIAN_H
#define GAUSSIAN_H

#include <cstdio>
#include <cstring>

#include "mymath.h"


template <int N>
class Gaussian {
public:
    double mu[N];
    double sigma_inv[N][N];
    double frac;

public:
    Gaussian () {
        double sigma[N][N] = {{0.0}};
        set_mu(sigma[0]);
        for (int i = 0; i < N; ++i) sigma[i][i] = 1.0;
        set_sigma(sigma);
    }

    Gaussian (double nmu[N], double nsigma[N][N]) {
        set_mu(nmu);
        set_sigma(nsigma);
    }

    bool set_mu(double nmu[N]) {
        memcpy(mu, nmu, sizeof mu);
        return true;
    }
    bool set_sigma(double sigma[N][N]);

    double p(double x[N]) const;
};

template <int N>
bool Gaussian<N>::set_sigma(double sigma[N][N]) {
    // Gaussian elimination

    double arr[N][N*2];

    for (int i = 0; i < N; ++i)
        for (int j = 0; j < N; ++j)
            arr[i][j] = sigma[i][j], arr[i][j+N] = i==j ? 1 : 0;

    for (int i = 0; i < N; ++i) {
        if (is_zero(arr[i][i])) return false;

        for (int j = i+1; j < N; ++j) {
            double mul = -arr[j][i] / arr[i][i];
            for (int k = 0; k < N * 2; ++k)
                arr[j][k] += arr[i][k] * mul;
        }
    }

    double det = 1.0;
    for (int i = 0; i < N; ++i) {
        det *= arr[i][i];

        double mul = 1.0 / arr[i][i];

        for (int j = 0; j < 2 * N; ++j)
            arr[i][j] *= mul;
    }

    for (int i = N-1; i >= 0; --i) {
        for (int j = i-1; j >= 0; --j) {
            double mul = -arr[j][i];

            for (int k = 0; k < 2 * N; ++k)
                arr[j][k] += arr[i][k] * mul;
        }
    }

    for (int i = 0; i < N; ++i)
        for (int j = 0; j < N; ++j)
            sigma_inv[i][j] = arr[i][j+N];

    frac = sqrt(det * pow(2 * M_PI, N));

    return true;
}

template <int N>
double Gaussian<N>::p(double x[N]) const {
    double xp[N];
    for (int i = 0; i < N; ++i)
        xp[i] = x[i] - mu[i];

    double exp_part = 0;
    for (int i = 0; i < N; ++i)
        for (int j = 0; j < N; ++j)
            exp_part += sigma_inv[i][j] * xp[i] * xp[j];
    exp_part *= -0.5;

    return exp(exp_part) / frac;
}

#endif
