#ifndef HISTO_H
#define HISTO_H

#include <cstdlib>
#include <cstring>

#include "mymath.h"

typedef unsigned char uint8;

static uint8 _ycbcr[3];
static uint8* _toycbcr(const uint8 *rgb) {
    memcpy(_ycbcr, to_ycbcr(rgb[0], rgb[1], rgb[2]), 3);

    return _ycbcr;
}

class Histogram {
public:
    double histo[256][256];

    void set_histogram(const uint8 pic[], const bool mask[], int N);

    double p(const uint8 *pxl) const {
        _toycbcr(pxl);
        return histo[_ycbcr[1]][_ycbcr[2]];
    }

private:
    void _transpose();
    void _1d_convolute();
};


#endif
