#include "histo.h"

#include <algorithm>

void Histogram::set_histogram(const uint8 pic[], const bool mask[], int N) {
    memset(histo, 0, sizeof(double) * 256 * 256);

    for (int i = 0; i < N; ++i) {
        if (not mask[i]) continue;

        _toycbcr(pic + i * 3);

        histo[_ycbcr[1]][_ycbcr[2]] += 1;
    }

    // convolute
    _1d_convolute();

    _transpose();
    _1d_convolute();
    _transpose();

    // normalize
    double sum = 0;
    for (int i = 0; i < 256; ++i)
        for (int j = 0; j < 256; ++j)
            sum += histo[i][j];

    double rep_sum = 1.0 / sum;
    for (int i = 0; i < 256; ++i)
        for (int j = 0; j < 256; ++j)
            histo[i][j] *= rep_sum;

    // duno what's this for but keep this here anyways
    for (int i = 0; i < 256; ++i)
        for (int j = 0; j < 256; ++j)
            if (is_zero(histo[i][j]))
                histo[i][j] = eps;
}

void Histogram::_transpose() {
    for (int i = 0; i < 256; ++i)
        for (int j = 0; j < i; ++j)
            std::swap(histo[i][j], histo[j][i]);
}

void Histogram::_1d_convolute() {
    double sigma = 0.8;

    double gauss[7];
    for (int i = -3; i <= 3; ++i)
        gauss[i+3] = exp(-0.5 * sqr((double) i / sigma));

    for (int i = 0; i < 256; ++i) {
        double tmp[256] = {0.0};

        for (int j = 0; j < 256; ++j)
            for (int k = -3; k <= 3; ++k)
                if (j + k >= 0 and j + k < 256)
                    tmp[j] += gauss[k+3] * histo[i][j];

        memcpy(histo[i], tmp, sizeof(double) * 256);
    }
}
